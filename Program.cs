﻿using DocuSign.eSign.Client;
using DocuSign.eSign.Client.Auth;
using DocuSignTemplateExample.Helpers;
using Microsoft.Extensions.Configuration;

var config = new ConfigurationBuilder()
    .AddJsonFile($"AppSettings.json", true, true)
    .Build();

try
{
    //Get accessToken before create and send envelope
    var accessToken = AuthHelper.AuthenticateWithJwt(config["DocuSign:ClientId"], config["DocuSign:UserId"],
        config["DocuSign:AuthServer"], File.ReadAllBytes(config["DocuSign:Key"]));
    
    var docuSignClient = new DocuSignClient();
    docuSignClient.SetOAuthBasePath(config["DocuSign:AuthServer"]);
    OAuth.UserInfo userInfo = docuSignClient.GetUserInfo(accessToken.access_token);
    OAuth.UserInfo.Account acct = userInfo.Accounts.FirstOrDefault();
    
    docuSignClient.Configuration.DefaultHeader.Add("Authorization", "Bearer " + accessToken);

    EnvelopeHelper.SendEnvelopeViaEmail(accessToken.access_token, acct.BaseUri + "/restapi", 
        acct.AccountId, config["DocuSign:TemplateId"]);
}
catch (Exception e)
{
    if (e.Message.Contains("consent_required"))
    {
        AuthHelper.GetConsent(config["DocuSign:UserId"],config["DocuSign:AuthServer"]);
    }
    
    Console.WriteLine(e);
    throw;
}

