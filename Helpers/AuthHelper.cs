using System.Diagnostics;
using DocuSign.eSign.Client;
using DocuSign.eSign.Client.Auth;

namespace DocuSignTemplateExample.Helpers;

internal class AuthHelper
{
    internal static OAuth.OAuthToken AuthenticateWithJwt(string clientId, string impersonatedUserId,
        string authServer, byte[] privateKeyBytes)
    {
        var docuSignClient = new DocuSignClient();
        var scopes = new List<string>
        {
            "signature",
            "impersonation",
        };

        return docuSignClient.RequestJWTUserToken(
            clientId,
            impersonatedUserId,
            authServer,
            privateKeyBytes,
            1,
            scopes);
    }
    
    internal static void GetConsent(string clientId, string authServer)
    {
        string caret = "";
        // build a URL to provide consent for this Integration Key and this userId
        string url = "https://" + authServer + "/oauth/auth?response_type=code" + caret + "&scope=impersonation%20signature" + caret +
                     "&client_id=" + clientId + caret + "&redirect_uri=" + "https://localhost";
        Console.WriteLine($"Consent is required - launching browser (URL is {url})");

        // Start new browser window for login and consent to this app by DocuSign user
        Process.Start("xdg-open", url);

        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Unable to send envelope; Exiting. Please rerun the console app once consent was provided");
        Console.ForegroundColor = ConsoleColor.White;
        Environment.Exit(-1);
    }
}