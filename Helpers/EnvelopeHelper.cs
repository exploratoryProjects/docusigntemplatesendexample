using DocuSign.eSign.Api;
using DocuSign.eSign.Client;
using DocuSign.eSign.Model;

namespace DocuSignTemplateExample.Helpers;

internal class EnvelopeHelper
{
    internal static string SendEnvelopeViaEmail(string accessToken, string basePath, string accountId, string templateId)
    {
        EnvelopeDefinition env = MakeEnvelope(templateId);
        
        var docuSignClient = new DocuSignClient(basePath);
        docuSignClient.Configuration.DefaultHeader.Add("Authorization", "Bearer " + accessToken);

        EnvelopesApi envelopesApi = new EnvelopesApi(docuSignClient);
        EnvelopeSummary results = envelopesApi.CreateEnvelope(accountId, env);
        ViewUrl signerUrl = envelopesApi.CreateRecipientView(accountId, results.EnvelopeId, signerViewRequest());
        return results.EnvelopeId;
    }
    
    private static EnvelopeDefinition MakeEnvelope(string templateId)
    {
        EnvelopeDefinition env = new();
        env.EmailSubject = "Please sign this document set";
        
        var patientId = new Text() { TabLabel = "patientId", Value = Guid.NewGuid().ToString() };

        var patientName = new Text() { TabLabel = "patientName", Value = "Patient Name Here" };

        var tabs = new Tabs() { TextTabs = new() { patientId, patientName } };

        var signer = new TemplateRole()
        {
            ClientUserId = "signer",
            Email = "templatesigner02@mailinator.com",
            Name = "Le Medico",
            RoleName = "medicalSigner",
            Tabs = tabs,
            EmbeddedRecipientStartURL = "SIGN_AT_DOCUSIGN",
        };
        var carbonCopy = new TemplateRole()
        {
            Email = "templatecarboncopy02@mailinator.com",
            Name = patientName.Value,
            RoleName = "patientViewer"
        };
        
        env.Status = "sent";
        env.TemplateId = templateId;
        env.TemplateRoles = new() { signer, carbonCopy };

        return env;
    }

    private static RecipientViewRequest signerViewRequest()
    {
        RecipientViewRequest viewRequest = new RecipientViewRequest();

        viewRequest.Email = "templatesigner02@mailinator.com";
        viewRequest.ClientUserId = "signer";
        viewRequest.UserName = "Le Medico";
        viewRequest.ReturnUrl = "https://postsign.docusign.com/postsigning/en/finish-signing";
        viewRequest.AuthenticationMethod = "none";

        return viewRequest;
    }
}